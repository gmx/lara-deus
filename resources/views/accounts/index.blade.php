@extends('layouts.app')
@section('content')
<h3>Accounts</h3>
<hr>

<div class="row">
	<div class="col-4">
		<form>
			<div class="form-group">
			<input type="text" name="q" class="form-control">
			<button class="btn btn-primary">Search</button>
			</div>
		</form>
	</div>
</div>

<a href="/accounts/create" class="btn btn-primary">New</a>
<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Category</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		@foreach($accounts as $record)
		<tr>
			<td>{{ $record->id }}</td>
			<td>{{ $record->title }}</td>
			<td>{{ $record->category->name }}</td>
			<td>
				<a href="/accounts/{{ $record->id }}/edit" class="btn btn-warning">Edit</a>
				<form action="/accounts/{{ $record->id }}" method="POST">
					<input type="hidden" name="_method" value="DELETE">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button class="btn btn-danger">Delete</button>
				</form>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection