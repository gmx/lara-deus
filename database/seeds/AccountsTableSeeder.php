<?php

use Illuminate\Database\Seeder;
use App\Account;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	Account::create([ 'title' => 'Transport', 'category' => 'Expense' ]);
       	Account::create([ 'title' => 'Food', 'category' => 'Expense' ]);
       	Account::create([ 'title' => 'Short Term Loan', 'category' => 'Liability' ]);
    }
}
