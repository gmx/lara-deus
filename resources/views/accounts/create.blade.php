@extends('layouts.app')
@section('content')
<h3>Create New Account</h3>
<hr>
<form method="POST" action="/accounts">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-group">
		<label for="exampleInputEmail1">Title</label>
		<input type="text" class="form-control" name="title" placeholder="Enter Account Title" value="{{ old('title') }}">
		<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
		<span style="color: red">{{ $errors->first('title') }}</span>
		@if (false)
			@foreach($errors->all() as $err)
			<span>{{ $err }}</span>
			@endforeach
		@endif
	</div>
	<div class="form-group">
		<label for="exampleInputPassword1">Category</label>
		<select name="category" id="" class="form-control">
			<option value="">Select Category</option>
			<option value="Asset">Asset</option>
			<option value="Expense">Expense</option>
			<option value="Liability">Liability</option>
			<option value="Equity">Equity</option>
		</select>	
	</div>

	<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection