<?php
 
namespace App\Filter;
use App\Account;

/**
 * 
 */
class AccountFilter
{
	private $_query;

	public function __construct($query)
	{
		$this->_query = $query;
	}

	public function setQuery( $query )
	{
		$this->_query = $query;	
	}
	
	public function run( $request )
	{
		return $this->_query->whereRaw('title like ?', [ "%$request->q%" ])->get();
	}	
}

