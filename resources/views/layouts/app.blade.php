<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Account</title>
	<!--
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	-->
	<link rel="stylesheet" href="/css/bootstrap.css">
<!-- 	<style>
		.col:nth-child(odd) { background-color: orange; }
		.col:nth-child(even) { background-color: red; }
		.col-2 { background: #2980b9 }
		.col-3 { background: #27ae60  }
	</style>
 --></head>
<body>
<div class="container">
	<div class="row">
		<div class="col-12">
			@yield('content')
		</div>
	</div>
</div>
@if (false)
<div class="container">
	<div class="row">
		<div class="col">1</div>
		<div class="col">2</div>
		<div class="col">3</div>
		<div class="col">4</div>
		<div class="col">5</div>
		<div class="col">6</div>
		<div class="col">7</div>
		<div class="col">8</div>
		<div class="col">9</div>
		<div class="col">10</div>
		<div class="col">11</div>
		<div class="col">12</div>
	</div>
	<div class="row">
		<div class="col-2">1-2</div>
		<div class="col-3">3-4-5</div>
	</div>
	<div class="row">
		<div class="col-md-3">Hello</div>
		<div class="col-3">World</div>
		<div class="col-3">Foo</div>
		<div class="col-3">Bar</div>
	</div>
</div>
@endif
</body>
</html>