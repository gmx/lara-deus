<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Filter\AccountFilter;
use App\Account;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind('App\Filter\AccountFilter', function ($app) {
            return new AccountFilter(Account::query());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
