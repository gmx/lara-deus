@extends('layouts.app')
@section('content')
<h3>Edit Account {{ $account->title }}</h3>
<hr>
<form method="POST" action="/accounts/{{ $account->id }}">
	<input type="hidden" name="_method" value="PUT">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-group">
		<label for="exampleInputEmail1">Title</label>
		<input type="text" class="form-control" name="title" 
			placeholder="Enter Account Title" value="{{ old('title', $account->title) }}">
		<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
		<span style="color: red">{{ $errors->first('title') }}</span>
	</div>
	<div class="form-group">
		<label for="exampleInputPassword1">Category</label>
		<select name="category" id="" class="form-control">
			<option value="">Select Category</option>
			@foreach([ 'Asset', 'Expense', 'Liability', 'Equity' ] as $record)
			<option value="{{ $record }}" {{ $record == $account->category ? 'selected' : '' }}>{{ $record }}</option>
			@endforeach
		</select>	
	</div>

	<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection