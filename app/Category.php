<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Account;

class Category extends Model
{
   	public $fillable = [ 'name' ];

   	// category has many accounts
   	public function accounts()
   	{
   		return $this->hasMany(Account::class);
   	}
}
