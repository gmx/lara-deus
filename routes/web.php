<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Account;

Route::get('/', function () {
	return view('layouts.app');
    // return view('welcome');
});

Route::bind('acc', function($id) {
	return Account::findOrFail( $id );
});

Route::get('/accounts', 'AccountController@index');
Route::get('/accounts/create', 'AccountController@create');
Route::post('/accounts', 'AccountController@store');

Route::get('/accounts/{acc}', 'AccountController@show');
Route::get('/accounts/{acc}/edit', 'AccountController@edit');
Route::put('/accounts/{acc}', 'AccountController@update');

Route::delete('/accounts/{acc}', 'AccountController@destroy');


