<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateOrUpdateAccountRequest;
use App\Account;
use App\Filter\AccountFilter;

class AccountController extends Controller
{
	public function index(Request $request, AccountFilter $filter) 
	{
		// $filter = new AccountFilter();
		// $filter->setQuery( Account::query() );
		$accounts = $filter->run( $request );

		return view( 'accounts.index', compact( 'accounts' ));
		// return view( 'accounts.index', [ 'accounts' => $accounts ]);
	}


	public function create() 
	{
		return view( 'accounts.create' );
	}

	// Dependency Injection
	// IoC Container
	public function store(CreateOrUpdateAccountRequest $request) 
	{
		// dump($request->all());
		// dump($request->except( '_token' ));
		$account = Account::create($request->except( '_token' ));
		return redirect('accounts');
	}

	public function show() {}
	public function edit(Account $account) 
	{
		return view( 'accounts.edit', compact( 'account' ));
	}

	public function update(Account $account, CreateOrUpdateAccountRequest $request) 
	{
		$account->update($request->except('_token'));
		return redirect('accounts');
	}
	
	/*	
	public function destroy($id) 
	{
		$account = Account::findOrFail($id);
		$account->delete();
		return redirect('accounts');
	}
	*/

	/*
	public function destroy(Request $request) 
	{
		dump($request->id);
	}
	*/

	public function destroy(Account $account) 
	{
		$account->delete();
		return redirect('accounts');
	}
}


