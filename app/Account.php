<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Account extends Model
{
 	public $fillable = [ 'title', 'category_id', 'type' ];

 	public $dates = [ 'created_at' ];

 	public function category()
 	{
 		return $this->belongsTo(Category::class);
 	}

 	// public function __get() {}
 	// accesser 
 	public function getAccountNoAttribute()
 	{
 		// year-month-day-category_id-id 
 		return $this->created_at->format('Y-m-d') . '-' . $this->attributes[ 'category_id' ]  . '-' . $this->attributes['id'];
 	}

 	public function setAccountNoAttribute($value)
 	{
 		echo $value;
 	}
}
